#PT-TOOLS

**Useful Firefox Borwser addons I use**:

  01. Cookies Manager+
  		https://addons.mozilla.org/en-US/firefox/addon/cookies-manager-plus/

  02. Firebug
  		https://addons.mozilla.org/en-US/firefox/addon/firebug/

  03. FlashFirebug
  		https://addons.mozilla.org/en-US/firefox/addon/flashfirebug/

  04. FoxyProxy
  		https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/

  05. HackBar
  		https://addons.mozilla.org/en-US/firefox/addon/hackbar/

  06. Live HTTP Headers**(REPLAY BUTTON FIX: http://www.mrt-prodz.com/blog/view/2014/09/fixing-live-http-headers-017-add-on)**
  		https://addons.mozilla.org/en-US/firefox/addon/live-http-headers/

  07. Wappalyzer
  		https://addons.mozilla.org/en-US/firefox/addon/wappalyzer/

  08. User Agent Switcher(xml with useragent strings included)
  		https://addons.mozilla.org/en-US/firefox/addon/user-agent-switcher/

  09. PassiveRecon
  		https://addons.mozilla.org/en-US/firefox/addon/passiverecon/

  10. NoScript
  		https://addons.mozilla.org/en-US/firefox/addon/noscript/

  11. Google Search Link Fix
  		https://addons.mozilla.org/en-US/firefox/addon/google-search-link-fix/

  12. Policeman
  		https://addons.mozilla.org/en-US/firefox/addon/policeman/

  13. Self-Destruction Cookies
  		https://addons.mozilla.org/en-US/firefox/addon/self-destructing-cookies/
